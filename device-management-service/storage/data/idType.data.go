type IDType int

const (
	IDTypeUndefined IDType = iota
	IDTypePath
	IDTypeCID
)