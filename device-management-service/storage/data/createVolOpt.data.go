package storage

// CreateVolumeOpt allows arbitrary operation on the storage volume
// while creating a new volume
type CreateVolOpt func(*dms.storage.StorageVolume)