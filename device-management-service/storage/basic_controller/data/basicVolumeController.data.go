package storage

// BasicVolumeController is the default implementation of the VolumeController.
// It persists storage volumes information in the local database.
type BasicVolumeController struct {
	// db is where all volumes information is stored.
	db *gorm.DB

	// basePath is the base path where volumes are stored under
	basePath string

	// file system to act upon
	fs afero.Fs
}
