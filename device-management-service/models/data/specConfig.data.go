package models

// SpecConfig represents a configuration for a spec
// A SpecConfig can be used to define an engine spec, a storage volume, etc.
type SpecConfig struct {
	// Type of the spec (e.g. docker, firecracker, storage, etc.)
	Type string `json:"type"`

	// Params of the spec
	// This allows passing arbitrary parameters to the spec implementation
	Params map[string]interface{} `json:"params,omitempty"`
}
