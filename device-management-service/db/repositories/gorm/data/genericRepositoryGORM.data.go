package database

// GenericRepositoryGORM is a generic repository implementation using GORM as an ORM.
// It is intended to be embedded in model repositories to provide basic database operations.

type GenericRepositoryGORM[T repositories.ModelType] struct {
	db *gorm.DB
}
