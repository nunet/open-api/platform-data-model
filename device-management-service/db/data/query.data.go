package database

// Query is a struct that wraps both the instance of type T and additional query parameters.
// It is used to construct queries with conditions, sorting, limiting, and offsetting.
type Query[T any] struct {
	// Instance is an optional object of type T used to build conditions from its fields.
	Instance T

	// Conditions represent the conditions applied to the query.
	Conditions []QueryCondition

	// SortBy specifies the field by which the query results should be sorted.
	SortBy string

	// Limit specifies the maximum number of results to return.
	Limit int

	// Offset specifies the number of results to skip before starting to return data.
	Offset int
}

// QueryCondition is a struct representing a query condition.
type QueryCondition struct {
	// Field specifies the database or struct field to which the condition applies.
	Field string

	// Operator defines the comparison operator (e.g., "=", ">", "<").
	Operator string

	// Value is the expected value for the given field.
	Value interface{}
}
