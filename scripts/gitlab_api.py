import requests
from slugify import slugify
import json

gitlabURI = 'https://gitlab.com/api/graphql'
gitlabToken = 'glpat-RczTBuN44UydXdrtxEiD'
gitlabWriteAccessToken = 'glpat-RczTBuN44UydXdrtxEiD'
gitlabHeaders = {"Authorization": "Bearer " + gitlabToken}
gitlabStatusCode = 200
gitlabPostStatusCode = 201

def run_query_text(uri: str, query, statusCode=gitlabStatusCode, headers=gitlabHeaders, method="GET"):
    request = requests.request(method, uri, json={'query': query}, headers=headers)
    if request.status_code == statusCode:
        return request.text
    else:
        if request.status_code == 404:
            return json.loads(request.content)
        else:
            raise Exception(f"Unexpected status code returned: {request.status_code}")

def run_query(uri: str, query, statusCode=gitlabStatusCode, headers=gitlabHeaders, method="POST"):
    request = requests.request(method, uri, json={'query': query}, headers=headers)
    if request.status_code == statusCode:
        return request.json()
    else:
        raise Exception(f"Unexpected status code returned: {request.status_code}")

def run_commit_query(projectId, data, statusCode=gitlabPostStatusCode, method="POST"):
    uri = "https://gitlab.com/api/v4/projects/"+projectId+"/repository/commits"
    headers = {}
    headers["Authorization"] =  "Bearer " + gitlabWriteAccessToken
    headers["Content-Type"] = "application/json"
    request = requests.request(method, uri, data=data, headers=headers)
    if request.status_code == statusCode:
        return request.json()
    else:
        if request.status_code == 400:
            return json.loads(request.content)
        else:
            raise Exception(f"Unexpected status code returned: {request.status_code}")

def getFileList(repo, fileName, branch):
    gitLabQuery = """\
      query {
        project(fullPath: """ + f'"{repo}"' + """) {
            id
            repository {
            tree(ref: """ + f'"{branch}"' + """, recursive: true){
                blobs{
                nodes{
                    name
                    type
                    path
                    sha
                }
                }
            }
            }
        }
        }
    """
    result = run_query(gitlabURI, gitLabQuery, gitlabStatusCode, gitlabHeaders)
    projectId = result["data"]["project"]["id"].split('/')[4]
    files = result["data"]["project"]["repository"]["tree"]["blobs"]["nodes"]
    readmes = list(filter(lambda c: fileName in c["name"], files))
    for readme in readmes:
        fileSHA = readme["sha"]
        fileContents = getFile(projectId,fileSHA)
        readme["raw"] = fileContents
    return [repo,projectId,readmes]

def getFile(projectId, fileSHA):
    restQuery = "https://gitlab.com/api/v4/projects/"+projectId+"/repository/blobs/"+fileSHA+"/raw"
    fileContents = run_query_text(restQuery, "", gitlabStatusCode, {"PRIVATE-TOKEN": gitlabToken}, "GET")
    return fileContents

def getFileByPath(projectId, filePath, branch):
    filePath = filePath.replace("/","%2F")
    restQuery = "https://gitlab.com/api/v4/projects/"+projectId+"/repository/files/"+filePath+"?ref="+branch
    response = run_query_text(restQuery, "", gitlabStatusCode, {"PRIVATE-TOKEN": gitlabToken}, "GET")
    return response

def fileExists(projectId, filePath, branch) -> bool:
    response = getFileByPath(projectId, filePath, branch)
    if type(response) == str:
        return True
    else:
        if response["message"] == "404 File Not Found":
            return False

def getIssueByName(issueName, projectName = "nunet/architecture"):
    gitLabQuery = """\
      query {
        project(fullPath: """  + f'"{projectName}"' + """) {
            issues(
                search: """  + f'"{issueName}"' + """) {
                nodes {
                    ...nodeProperties
                    }
            }
      }
    }

    fragment nodeProperties on Issue {
        id
        webUrl
        title
        iteration {title}
        epic {id title}
        labels {nodes {title}}
        assignees {nodes {name}}
        updatedBy {name}
        updatedAt
        weight
        state
        dueDate
        blockedByIssues {
          nodes {
            id
          }
        }
    }
    """
    result = run_query(gitlabURI, gitLabQuery, gitlabStatusCode, gitlabHeaders)
    issues = result['data']['project']['issues']['nodes']
    return issues

def getMilestoneById(milestone_id):
    gitLabQuery = """\
      query {
        milestone(
            id: """  + f'"{milestone_id}"' + """) {
                ...nodeProperties
            }
    }

    fragment nodeProperties on Milestone {
        createdAt
        description
        dueDate
        expired
        startDate
        state
        stats {closedIssuesCount, totalIssuesCount}
        title
        updatedAt
        webPath
    }
    """
    result = run_query(gitlabURI, gitLabQuery, gitlabStatusCode, gitlabHeaders)
    milestone = result['data']['milestone']
    return milestone    

def getIssueById(issue_id):
    gitLabQuery = """\
      query {
        issue(
            id: """  + f'"{issue_id}"' + """) {
                ...nodeProperties
            }
    }

    fragment nodeProperties on Issue {
        id
        webUrl
        title
        iteration {title}
        epic {id title}
        labels {nodes {title}}
        assignees {nodes {name}}
        updatedBy {name}
        updatedAt
        weight
        state
        dueDate
        milestone {id}
        blockedByIssues {
          nodes {
            id
          }
        }
    }
    """
    result = run_query(gitlabURI, gitLabQuery, gitlabStatusCode, gitlabHeaders)
    issue = result['data']['issue']
    return issue

def getProjectId(fullPath):
    gitLabQuery = """\
        query {
            project(fullPath: """ + f'"{fullPath}"' + """) {
                fullPath
                id
            }
        }
    """
    result = run_query(gitlabURI, gitLabQuery, gitlabStatusCode, gitlabHeaders)
    projectId = result["data"]["project"]["id"].split('/')[4]
    return projectId

def getAllIssuesFirstPage(params):
    gitlabQuery = """\
      query {
        group(fullPath: "nunet") {
            issues(first: 100, 
                includeSubgroups: true,
                state: """  + f'{params["state"]}' + """) {
                nodes {
                    ...nodeProperties
                    }
                pageInfo {
                    endCursor
                    hasNextPage
                }
            }
      }
    }

    fragment nodeProperties on Issue {
        id
        webUrl
        title
        iteration {title}
        epic {id title}
        labels {nodes {title}}
        assignees {nodes {name}}
        updatedBy {name}
        updatedAt
        weight
        state
        dueDate
        blockedByIssues {
          nodes {
            id
          }
        }
    }
    """
    result = run_query(gitlabURI, gitlabQuery, gitlabStatusCode, gitlabHeaders)
    response = {}
    
    response['hasNextPage'] = result['data']['group']['issues']['pageInfo']['hasNextPage']
    response['endCursor'] = result['data']['group']['issues']['pageInfo']['endCursor']

    issues = result['data']['group']['issues']['nodes']
    issue_map = {}
    for issue in issues:
        issue_map[issue['id']] = issue

    response['issue_map'] = issue_map

    return(response)


def getAllIssuesNextPage(params, cursorPosition):
    gitlabQuery = """\
      query {
        group(fullPath: "nunet") {
            issues(first: 100, 
                    after: """ + f'"{cursorPosition}"' + """, 
                    includeSubgroups: true,
                    state:"""  + f'{params["state"]}' + """) {
                nodes {
                    ...nodeProperties
                }
                pageInfo {
                    endCursor
                    hasNextPage
                }
            }
        }
    }

    fragment nodeProperties on Issue {
        id
        webUrl
        title
        iteration {title}
        epic {id title}
        labels {nodes {title}}
        assignees {nodes {name}}
        updatedBy {name}
        updatedAt
        weight
        state
        dueDate
        blockedByIssues {
          nodes {
            id
          }
        }
    }
    """
    result = run_query(gitlabURI, gitlabQuery, gitlabStatusCode, gitlabHeaders)
    response = {}

    response['hasNextPage'] = result['data']['group']['issues']['pageInfo']['hasNextPage']
    response['endCursor'] = result['data']['group']['issues']['pageInfo']['endCursor']

    issues = result['data']['group']['issues']['nodes']
    issue_map = {}
    for issue in issues:
        issue_map[issue['id']] = issue

    response['issue_map'] = issue_map
    
    return(response)

def getAllIssues(params):
    page = getAllIssuesFirstPage(params)
    all_issues = page['issue_map']
    while page['hasNextPage']:
        page = getAllIssuesNextPage(params,page['endCursor'])
        all_issues.update(page['issue_map'])
    
    return(all_issues)

def new_commit_message(branch, message):
    payload = {}
    payload["branch"] = branch
    payload["commit_message"] = message
    payload["actions"] = []
    return payload

def add_file_to_commmit(payload, file_path, content, projectId, branch):
    file = fileExists(projectId, file_path, branch)
    actionType = ""
    if file: 
        actionType = "update"
    else: 
        actionType = "create"
    action = {}
    action["action"] = actionType
    action["file_path"] = file_path
    action["content"] = content
    payload["actions"].append(action)
    return payload

def getActiveMilestones():
    milestones = run_query("https://gitlab.com/api/v4/groups/6160918/milestones?state=active", "", gitlabStatusCode, {"PRIVATE-TOKEN": gitlabToken}, "GET")
    return milestones

def getMilestoneGoal(milestoneName, label):
    gitLabQuery = """\
      query {
        group(fullPath: "nunet") {
            issues(
                labelName: """  + f'"{label}"' + """,
                milestoneTitle: """  + f'"{milestoneName}"' + """) {
                nodes {
                    ...nodeProperties
                    }
            }
      }
    }

    fragment nodeProperties on Issue {
        id
        webUrl
        title
        iteration {title}
        epic {id title}
        labels {nodes {title}}
        assignees {nodes {name}}
        updatedBy {name}
        updatedAt
        weight
        state
        dueDate
        milestone {id}
        blockedByIssues {
          nodes {
            id
          }
        }
    }
    """
    result = run_query(gitlabURI, gitLabQuery, gitlabStatusCode, gitlabHeaders)
    issues = result['data']['group']['issues']['nodes']
    return issues

def getMilestoneGoals(label):
    gitLabQuery = """\
      query {
        group(fullPath: "nunet") {
            issues(
                labelName: """  + f'"{label}"' + """) {
                nodes {
                    ...nodeProperties
                    }
            }
      }
    }

    fragment nodeProperties on Issue {
        id
        webUrl
        title
        iteration {title}
        epic {id title}
        labels {nodes {title}}
        assignees {nodes {name}}
        updatedBy {name}
        updatedAt
        weight
        state
        dueDate
        milestone {id}
        blockedByIssues {
          nodes {
            id
          }
        }
    }
    """
    result = run_query(gitlabURI, gitLabQuery, gitlabStatusCode, gitlabHeaders)
    issues = result['data']['group']['issues']['nodes']
    return issues
