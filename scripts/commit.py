import gitlab_api as gl
import os
import subprocess

def convertMermaidToSVG(filePathInput, filePathOutput, configPath):
    print("Converting {} ...".format(filePathInput))
    print("... to {}".format(filePathOutput))
    configParam = configPath +"/mermaid-config.json"
    puppeteerParam = configPath +"/puppeteer-config.json" 
    inputParam = filePathInput
    outputParam = filePathOutput
    # for usage on remove pipeline:
    process = subprocess.Popen(["/home/mermaidcli/node_modules/.bin/mmdc", "--configFile", configParam, "-p", puppeteerParam, "-i", inputParam, "-o",outputParam], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # for local testing
    # process = subprocess.Popen(["mmdc", "--configFile", configParam, "-p", puppeteerParam, "-i", inputParam, "-o",outputParam], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    return (process,stdout,stderr)

def collectFilesToCommit():
    ws = os.path.dirname(os.path.realpath(__file__))
    print("Workingin directory set to: {}".format(ws))
    components = open(ws+"/modules.txt", 'r').read().split('\n')
    print('Found {} components to process: {}'.format(len(components),components))
    commitFiles = []
    for component in components:
        print("Processing component {}".format(component))
        relPath0 = component
        print("At relative path {}".format(relPath0))
        absPath0 = os.path.abspath(os.path.join(ws, '..', relPath0))
        print('At absolute path {}'.format(absPath0))
        packages = next(os.walk(absPath0))[1]
        print("Found {} packages: {}".format(len(packages), packages))
        for package in packages:
            print("Processing module {}".format(package))
            relPath1 = relPath0 + "/" + package
            print("At relative path {}".format(relPath1))
            absPath1 = os.path.abspath(os.path.join(ws, '..', relPath1))
            print('At absolute path {}'.format(absPath1))                
            specCats = next(os.walk(absPath1))[1]
            print("Found {} categories: {}".format(len(specCats),specCats))
            for category in specCats:
                print("Processing category {}".format(category))
                relPath2 = relPath1 + "/" + category
                print("At relative path {}".format(relPath2))
                absPath2 = os.path.abspath(os.path.join(ws, '..', relPath2))
                print('At absolute path {}'.format(absPath2))                
                fileList = os.listdir(absPath2)
                for fileName in fileList:
                    if '.mermaid' in fileName:
                        filePathSource = absPath2 + "/" + fileName
                        outputDir = absPath2 + "/rendered"
                        if not os.path.exists(outputDir):
                            os.mkdir(outputDir)
                        filePathResultTemp =  outputDir + "/" + fileName
                        filePathResult = os.path.splitext(filePathResultTemp)[0]+'.svg'
                        configPath = os.path.join(ws, '..', "python-mermaid-docker")
                        result = convertMermaidToSVG(filePathSource, filePathResult, configPath)
                        print('Adding {} to file list for commit'.format(filePathResult))
                        with open(filePathResult) as f:
                            raw = f.read()
                        entry = {}
                        commitPathTemp = relPath2+'/rendered/'+fileName
                        commitPath = os.path.splitext(commitPathTemp)[0]+'.svg'
                        print('To be commited to directory {}'.format(commitPath))
                        entry['fileName'] = commitPath
                        entry['raw'] = raw 
                        commitFiles.append(entry)
    commitSize = len(commitFiles)
    print("{} files set for commit".format(commitSize))
    return commitFiles

def commitFiles(projectName, branch, files):
    projectId = gl.getProjectId(projectName)
    commit = gl.new_commit_message(branch, "[ci-skip] automatically rendered files from mermaid sources")
    for file in files:
        commit = gl.add_file_to_commmit(commit, file['fileName'], file['raw'], projectId, branch)
        print("Added file {} to commit message".format(file['fileName']))
    response = None
    #print("Performing commit to project {}: {}".format(projectName, commit))
    if len(commit['actions'])>0:
        data = gl.json.dumps(commit)
        response  = gl.run_commit_query(projectId, data)
    return response


def run(projectName, branch):
     filesToCommit = collectFilesToCommit()
     commitFiles(projectName,branch,filesToCommit)

# TODO: add propoer logging class with logging levels to handle debugging messages (now just commented out)
# TODO: clean /rendered/ directory each time things are pushed; otherwise it will keep all old files and renders; 
