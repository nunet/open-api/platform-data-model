# platform-data-model

This repository contains full data models and they renders that are used in architecture descriptions and specifications of all branches in development and production. Specifications provided here follow component, repository structure and documentation of the platorm as follows:

`./{repository_name}/{package_name}/{category_name}/{specification_name}{category_name}.mermaid`

Here:
* `repository_name` corresponds both to component name of the platform and repository name as used in gitlab, which should be exactly the same;
* `packaga_name` is name of package of a component (if that component has more than one package, which depends on the architecture of a component)
* `category_name` is related to the categories of specifications -- currenty there are three categories: 
    * `data` (data at rest);
    * `messages` (payload of messages in transit);
    * `sequences` (descriptions of processes as sequence diagrams)
    * (more can be added by simply putting additional directories)
* `spacification_name` is the name of each specification. Note, that file names also include category name as primary extention.

The above directory should contain mermaid files of specifications. When new specifications are pushed to the repo, the pipeline automatically renders all mermaid files into svg files and pushes to the chile directory of the same module `./{repository_name}/{module_name}/{category_name}/{specification_name}.{category_name}.svg'. 

To render specification files and commit them into the repo, each component (with specifications) should be registered in `scripts/modules.txt` file in the format `{repository_name}`. If a new specifications are not registered there, the rendering script in the pipeline will not pick those sources and will not render them.

**Imoportant** The directory structure and naming conventions have to be followed precisely, because specification files may be pulled and rendered automatically and used in platfrom technical documentation further down the line. We plan to build and render at least part of technical documentation automatically using the repository structure.

